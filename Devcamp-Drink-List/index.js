// Câu lệnh tương tự câu lệnh import express from 'express'; Dùng để import thư viện express vào project
const { drinkList } = require('./drink.js');

// const { response } = require('express');
const express = require('express');
// const res = require('express/lib/response');/

//Khởi tao app express
const app = express();

//Khai báo cổng của project
const port = 8000;


var drink_1 = new drinkList('1', 'TRATAC', 'Trà tắc', 10000, '14/5/2021', '14/5/2021');
var drink_2 = new drinkList('2', 'COCA', 'Cocacola', 15000, '14/5/2021', '14/5/2021');
var drink_3 = new drinkList('3', 'PEPSI', 'pepsi', 15000, '14/5/2021', '14/5/2021');

var listDrink = [drink_1, drink_2, drink_3]

// Khai báo api dạng get “/drinks-class sẽ chạy vào đây
app.get("/drink-class", (request, response) => {
    response.status(200).json({
        drink: listDrink
    })
});

//  TASK 2: LẤY API BẰNG CODE CHO CLASS
app.get('/drink-class/search', (request, response) => {
    let query = request.query

    if (query.codeDrink == "") {
        response.status(200).json({
            drink: [drink_1.getDrink(), drink_2.getDrink(), drink_3.getDrink()]
        })

    } else {
        listDrink.filter(paramCodeDrink => {
            if (query.codeDrink == paramCodeDrink.getDrinkCode()) {
                response.status(200).json({
                    DrinkClassMessage: paramCodeDrink.getDrink()
                })
            }
        });

        listDrink.filter(paramCodeDrink => {
            if (query.codeDrink !== paramCodeDrink.getDrinkCode()) {
                response.status(200).json({
                    DrinkClassMessage: "Thông tin đồ uống sai!!!"
                })
            }
        })
    }
})


//  TASK 4: LẤY API BẰNG ID CHO CLASS
app.get("/drink-class/:param1", (request, response) => {
    let param1 = request.params.param1;

    listDrink.filter(paramID => {
        if (param1 == paramID.getDrinkId()) {
            response.status(200).json({
                DrinkClassMessage: paramID.getDrink()
            })
        }
    })

    listDrink.filter(paramID => {
        if (param1 !== paramID.getDrinkId()) {
            response.status(200).json({
                DrinkClassMessage: "thông tin đồ uống sai!"
            })
        }
    })
})



// Khai báo api dạng get “/drinks-class sẽ chạy vào đây
app.get("/drink-object", (request, response) => {
    const listDrink = [];
    listDrink.push(drink_1, drink_2, drink_3);
    response.status(200).json(listDrink);
});


//  TASK 3: LẤY API BẰNG CODE CHO OBJECT
app.get('/drink-object/search', (request, response) => {
    let query = request.query

    if (query.codeDrink == "") {
        response.status(200).json({
            drink: [drink_1.getDrink(), drink_2.getDrink(), drink_3.getDrink()]
        })

    } else {
        listDrink.filter(paramCodeDrink => {
            if (query.codeDrink == paramCodeDrink.getDrinkCode()) {
                response.status(200).json({
                    DrinkObjectMessage: paramCodeDrink.getDrink()
                })
            }
        });

        listDrink.filter(paramCodeDrink => {
            if (query.codeDrink !== paramCodeDrink.getDrinkCode()) {
                response.status(200).json({
                    DrinkObjectMessage: "Thông tin đồ uống sai!!!"
                })
            }
        })
    }
})


//  TASK 5: LẤY API BẰNG ID CHO OBJECT
app.get("/drink-object/:param2", (request, response) => {
        let param2 = request.params.param2;

        listDrink.filter(paramID => {
            if (param2 == paramID.getDrinkId()) {
                response.status(200).json({
                    DrinkObjectMessage: paramID.getDrink()
                })
            }
        })

        listDrink.filter(paramID => {
            if (param2 !== paramID.getDrinkId()) {
                response.status(200).json({
                    DrinkObjectMessage: "thông tin đồ uống sai!"
                })
            }
        })
    })
    //Chạy app express
app.listen(port, () => {
    console.log("App listening on port (Ứng dụng đang chạy trên cổng) " + port);

})