class drinkList {
    constructor(paramID, maNuocUong, tenNuocUong, donGia, ngayTao, ngayCapNhat) {
        this.Id = paramID;
        this.codeDrink = maNuocUong;
        this.tenNuocUong = tenNuocUong;
        this.donGia = donGia;
        this.ngayTao = ngayTao;
        this.ngayCapNhat = ngayCapNhat;
    }
    getDrink() {
        return {
            "maNuocUong": this.codeDrink,
            "tenNuocUong": this.tenNuocUong,
            "donGia": this.donGia,
            "ngayTao": this.ngayTao,
            "ngayCapNhat": this.ngayCapNhat
        }
    }
    getDrinkCode() {
        return this.codeDrink
    }
    getDrinkId() {
        return this.Id
    }
}
module.exports = { drinkList };