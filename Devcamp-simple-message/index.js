// Câu lệnh tương tự câu lệnh import express from 'express'; Dùng để import thư viện express vào project
const express = require('express');

const app = express();

//Khai báo cổng của project
const port = 8000;

//Khai báo API dạng get "/" sẽ chạy vào đây
// Callback function: Là 1 thâm số của hàm khác và nó sẽ được thực thi ngay sau khi dàm đấy được gọi
app.get("/", (request, response) => {
    let today = new Date();
response.send(`Xin chào, hôm nay là ngày  ${today.getDate()}, tháng ${today.getMonth() + 1}, năm ${today.getFullYear()}`)
})

//Chạy app express
app.listen(port, () => {
    console.log("App listening on port (Ứng dụng đang chạy trên cổng) " + port);

})