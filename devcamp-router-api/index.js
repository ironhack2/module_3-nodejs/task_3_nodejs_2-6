// Câu lệnh tương tự câu lệnh import express from 'express'; Dùng để import thư viện express vào project
const express = require('express');

//Khởi tao app express
const app = express();

//Khai báo cổng của project
const port = 8000;

//Khai báo API dạng get "/" sẽ chạy vào đây
// Callback function: Là 1 thâm số của hàm khác và nó sẽ được thực thi ngay sau khi dàm đấy được gọi
app.get("/", (request, response) => {
    let today = new Date();

    response.status(200).json({
        message: `Xin chào, hôm này là ngày ${today.getDate()} tháng ${today.getMonth()} ${today.getFullYear()}`
    })
})

app.get("/get-method", (request, response) => {
    response.status(200).json({
        info: "Get method"
    })
})

app.post("/post-method", (request, response) => {
    response.status(200).json({
        info: "Post method"
    })
})

app.put("/put-method", (request, response) => {
    response.status(200).json({
        info: "Put method"
    })
})

app.delete("/delete-method", (request, response) => {
    response.status(200).json({
        info: "Delete method"
    })
})


//Chạy app express
app.listen(port, () => {
    console.log("App listening on port (Ứng dụng đang chạy trên cổng) " + port);

})